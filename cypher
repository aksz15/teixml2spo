:auto
MATCH (n)
WITH n
CALL {
WITH n
DETACH DELETE n}
IN TRANSACTIONS OF 1000 ROWS;

CALL apoc.schema.assert({},{},true) YIELD label, key
RETURN *;

WITH atag.text.load
('https://git.thm.de/aksz15/teixml2spo/-/raw/master/patzig.html') as t
create (t2:Text {id:1, html:t})
return *;

MATCH (t:Text {id:1})
CALL atag.text.import.html(t, 'html', 'Annotation', 'text', 'HAS_ANNOTATION') YIELD node
RETURN node;

match (m:Annotation {tag:'margin', text:'Aehnlichkeit'})
create (t:Text {text:'Erstes System characteristische Merkmale'})
create (a:Annotation {tag:'expan', type:'orig', text:'characteristische', startIndex:'14', endIndex:'31'})
create (t2:Text {text:'charakterist.'})
with t, a, m, t2
create (m)-[:HAS_TEXT]->(t)
create (t)-[:HAS_ANNOTATION]->(a)
create (a)-[:HAS_TEXT]->(t2);

match (m:Annotation {tag:'margin', text:'Planeten von der Sonne'})
create (t:Text {text:'Zwei besondere Planeten-Systeme'})
create (a:Annotation {tag:'u', startIndex:'0', endIndex:'31'})
with t, a, m
create (m)-[:HAS_TEXT]->(t)
create (t)-[:HAS_ANNOTATION]->(a)
create (a)-[:HAS_TEXT]->(t2);

match (t:Text)
where t.text is not null
with t
CALL atag.chains.characterChain(t.text) yield path return path;

WITH atag.text.load('https://git.thm.de/aksz15/teixml2spo/-/raw/master/patzig.xml?inline=false') as xml
CREATE (t:Text)
SET t.xml = xml;


